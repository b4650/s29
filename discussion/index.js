// this require("express") allows devs to load/import express package that will be used for the application
let express = require("express")

const app = express()
const port = 3000


	// use function lets the middleware to do common services and capabilities to the applications
app.use(express.json());
 // lets the app to read json data
app.use(express.urlencoded({extended : true }));
 // allows the app to receive data from the forms
	// not using these will result to our req.body to return undefined

// express has methods coprresponding to each http method (get, post, put, delete, etc)
// this "/" route expects to receive a get reqauest at its endpoint (http://localhost3000/)

app.get("/", (req, res) => {
	// res.send - allows sending of message as a responses to the client
	res.send("Hello World")
	})


app.get("/hello", (req, res) => {
	res.send("hello from the /hello endpoint")
	})


app.post("/hello", (req, res) => {
	console.log(req.body.firstName)
	console.log(req.body.lastName)
	// send the response once the req.body.firstName and req.body.lastName has values
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`)
	})


app.listen(port, () => console.log(`Server is running at port ${port}`));

// ACTIVITY

let users = [{
	"username" : "jojo123",
	"password" : "MaritesLangMahalKo"
}]

app.get("/home", (req, res) => {
	res.send("Welcome to the home page")
	});

app.get("/users", (req, res) => {
	res.send(users)
	});

app.delete("/delete-user", (req, res) => {
	res.send(`User ${req.body.username} has been deleted.`)
	});
